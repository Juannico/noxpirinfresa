using UnityEngine;

public class GroundHandler : MonoBehaviour
{
    [SerializeField] private LayerData _layerData;

    private bool _isGrounded = true;
    public bool IsGrounded => _isGrounded;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (LayerUtilities.IsSameLayer(_layerData.enviromentLayer, collision.gameObject.layer))
            _isGrounded = true;
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (LayerUtilities.IsSameLayer(_layerData.enviromentLayer, collision.gameObject.layer))
            _isGrounded = false;
    }

}
