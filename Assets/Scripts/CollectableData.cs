using UnityEngine;

[CreateAssetMenu(fileName = "SO_CollectableData", menuName = "ScriptableObjects/CollectableData", order = 1)]
public class CollectableData : ScriptableObject, ISerializationCallbackReceiver
{
    public int maxAmountInPossesion = 3;
    public Sprite sprite;
    [HideInInspector] public int amountCollected;
    private int _amount;
    public int Amount
    {
        get => _amount;
        set
        {
            if (_amount < value)
                amountCollected += value - _amount;
            _amount = value;
            if (_amount < 0)
                _amount = 0;
            OnAmountChanged.Invoke(value);
        }
    }
    public delegate void IntChanged(int intToChange);
    public IntChanged OnAmountChanged;
    public bool CanCollect() => _amount < maxAmountInPossesion;
    public void RestartData()
    {
        _amount = 0;
        amountCollected = 0;
        OnAmountChanged = null;
    }
    public void OnAfterDeserialize()
    {

    }

    public void OnBeforeSerialize()
    {
        RestartData();
    }
}
