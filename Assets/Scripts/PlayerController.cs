using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private LayerData _layerData;
    [SerializeField] private float _speed;
    [SerializeField] private float _hightOffset = 0.35f;
    [SerializeField] private Transform _objectToRotate;
    [SerializeField] private Animator _animator;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private float _jumpForce;
    [SerializeField] private float _touchYDistance = 15;
    [SerializeField] private GroundHandler _groundHandler;
    [Header("Fx")]
    [SerializeField] private ParticleSystem _dustEffect;
    [SerializeField] private AudioSource _walkSoundEffect;
    private float _lastPointPositionX;
    private float _lastDirection = 1;
    private bool _moving;
    [SerializeField] private bool _canMove;
    private Vector3 _startDustPosition;
    private float _lastPointPositionY;
    private Rigidbody2D _rb2D;
    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _rb2D = GetComponent<Rigidbody2D>();
        SetMoveActions(true);
        _startDustPosition = _dustEffect.transform.localPosition;
    }
    private void Update()
    {
        if (!_moving)
        {
            _lastPointPositionY = InputManager.Instance.pointPosition.action.ReadValue<Vector2>().y;
            return;
        }
        Move();
        Jump();
        _canMove = true;
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (!_moving)
            return;
        if (!LayerUtilities.IsSameLayer(_layerData.obstacleLayer, collision.gameObject.layer))
            return;
        if (transform.position.y - collision.contacts[0].point.y - _hightOffset > 0)
            return;
        float collisionDirection = collision.transform.position.x > transform.position.x ? 1 : -1;
        if (collisionDirection == _lastDirection)
            _canMove = false;
    }

    private void OnDestroy()
    {
        SetMoveActions(false);
    }
    private void Move()
    {
        float currentPointPositionX = InputManager.Instance.pointPosition.action.ReadValue<Vector2>().x;
        float direction = _lastDirection;
        if (_lastPointPositionX != currentPointPositionX)
            direction = _lastPointPositionX < currentPointPositionX ? 1 : -1;
        if (_canMove)
            _objectToRotate.Rotate(Vector3.forward * _speed * Time.deltaTime * direction);
        ShoulChangeDirection(direction);
        _lastPointPositionX = currentPointPositionX;
        _lastDirection = direction;
        Vector3 rotaion = Vector3.zero;
        if (direction < 0)
            rotaion.y = 180;
        _dustEffect.transform.localRotation = Quaternion.Euler(rotaion);
        Vector3 dustPosition = _startDustPosition;
        dustPosition.x *= direction;
        _dustEffect.transform.localPosition = dustPosition;

    }
    private void Jump()
    {
        if (!_groundHandler.IsGrounded)
        {
            _lastPointPositionY = InputManager.Instance.pointPosition.action.ReadValue<Vector2>().y;
            return;
        }


        _lastPointPositionY = InputManager.Instance.pointPosition.action.ReadValue<Vector2>().y;
    }
    private void ShoulChangeDirection(float direction)
    {
        if (_lastDirection == direction)
            return;
        _spriteRenderer.flipX = direction < 0;
    }

    private void StarMoveCallBackContext(InputAction.CallbackContext ctx)
    {
        _lastPointPositionX = InputManager.Instance.pointPosition.action.ReadValue<Vector2>().x;
        _moving = true;
        _animator.SetBool("isWalking", _moving);
        _dustEffect.Play();
        _walkSoundEffect.Play();
        _lastPointPositionY = InputManager.Instance.pointPosition.action.ReadValue<Vector2>().y;
    }

    private void StopMoveCallBackContext(InputAction.CallbackContext ctx)
    {
        _lastPointPositionX = InputManager.Instance.pointPosition.action.ReadValue<Vector2>().x;
        _moving = false;
        _animator.SetBool("isWalking", _moving);
        _dustEffect.Stop();
        _walkSoundEffect.Stop();
        float currentPointPositionY = InputManager.Instance.pointPosition.action.ReadValue<Vector2>().y;
        if (currentPointPositionY > _lastPointPositionY + _touchYDistance && _groundHandler.IsGrounded)
            _rb2D.AddForce(Vector2.up * _jumpForce, ForceMode2D.Impulse);
    }

    public void SetMoveActions(bool add)
    {
        if (add)
        {
            InputManager.Instance.move.action.performed += StarMoveCallBackContext;
            InputManager.Instance.move.action.canceled += StopMoveCallBackContext;
            return;
        }
        InputManager.Instance.move.action.performed -= StarMoveCallBackContext;
        InputManager.Instance.move.action.canceled -= StopMoveCallBackContext;
    }
}
