using System;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private LayerData _layerData;
    [SerializeField] private float _speed;
    [SerializeField] private GameObject _trailRender;
    [SerializeField] public SpriteRenderer _spriteRenderer;
    [Header("Fx")]
    [SerializeField] private ParticleSystem _explodeEffect;
    [SerializeField] private AudioSource _explodeSoundEffect;
    private Animator _cameraAnimator;

    public Rigidbody2D Rb2D { get; private set; }
    private Action _onStopThrowing;
    private void Awake()
    {
        _cameraAnimator = Camera.main.GetComponent<Animator>();
        Rb2D = gameObject.GetComponent<Rigidbody2D>();
        _spriteRenderer.enabled = false;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (LayerUtilities.IsSameLayer(_layerData.playerLayer, other.gameObject.layer))
            return;
        if (LayerUtilities.IsSameLayer(_layerData.enviromentLayer, other.gameObject.layer))
        {
            _cameraAnimator.SetTrigger("hardShake");
            StopTrow(true);
            return;
        }
        if (other.TryGetComponent(out GravitationField gravitationField))
            gravitationField.ProjectileEnter(this);
        if (other.TryGetComponent(out Enemy enemy))
        {
            _cameraAnimator.SetTrigger("hardShake");
            enemy.Kill();
            StopTrow(true);
        }
    }
    public void Prepare()
    {
        enabled = false;
        _spriteRenderer.enabled = true;
        _trailRender.SetActive(false);
    }
    public void Throw(Vector2 direction, Action onStopThrowing = null, Vector2 startPosition = default)
    {
        _cameraAnimator.SetTrigger("softShake");
        UiManager.Instance.SetEnterTowwerButtonState(false);
        Rb2D.velocity = Vector2.zero;
        enabled = true;
        _spriteRenderer.enabled = true;
        _trailRender.SetActive(true);
        if (startPosition != default)
            transform.position = startPosition;
        transform.up = direction;
        Rb2D.AddForce(direction.normalized * _speed, ForceMode2D.Impulse);
        if (onStopThrowing != null)
            _onStopThrowing = onStopThrowing;
    }
    public void PauseTrow() => Rb2D.velocity = Vector2.zero;
    public void StopTrow(bool doEffect = false)
    {
        Rb2D.velocity = Vector2.zero;
        _spriteRenderer.enabled = false;
        enabled = false;
        if (doEffect)
        {
            UiManager.Instance.SetEnterTowwerButtonState(true);
            _explodeEffect?.Play();
            _explodeSoundEffect?.Play();
        }
        _onStopThrowing?.Invoke();
        _onStopThrowing = null;
    }

}

