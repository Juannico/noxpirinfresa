using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    public InputActionReference shoot;
    public InputActionReference move;
    public InputActionReference pointPosition;
    private static InputManager _instance;
    public static InputManager Instance => _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this);
            return;
        }
        _instance = this;
        DontDestroyOnLoad(this);
        SetActionState(shoot.action, true);
        SetActionState(move.action, true);
        SetActionState(pointPosition.action, true);
        GameManager.Instance.OnPauseChange += SetActionOnPause;
    }
    private void OnDestroy()
    {
        if (Instance != this)
            return;
        SetActionState(shoot.action, false);
        SetActionState(move.action, false);
        SetActionState(pointPosition.action, false);

        GameManager.Instance.OnPauseChange -= SetActionOnPause;
    }
    private void SetActionOnPause(bool PauseState)
    {
        SetActionState(shoot.action, !PauseState);
        SetActionState(move.action, !PauseState);
        SetActionState(pointPosition.action, !PauseState);
    }
    public void SetActionState(InputAction actionToSet, bool enable)
    {
        if (enable)
        {
            actionToSet.Enable();
            return;
        }
        actionToSet.Disable();
    }
}

