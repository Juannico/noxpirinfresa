using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UiManager : MonoBehaviour
{
    [SerializeField] private Button _enterTowerButton;
    [SerializeField] private EndGameUI _endGameUI;
    [SerializeField] private GameObject _startUI;
    [SerializeField] private GameObject _instructionsUI;
    private static UiManager _instance;
    public static UiManager Instance => _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }
        _instance = this;
        DontDestroyOnLoad(this);
        _enterTowerButton.gameObject.SetActive(false);
        _startUI.SetActive(true);
        _instructionsUI.SetActive(false);
    }
    public void SetEnterTowwerButtonState(bool buttonActivated) => _enterTowerButton.gameObject.SetActive(buttonActivated);

    public void StartGame()
    {
        _startUI.SetActive(false);
        _instructionsUI.SetActive(true);
    }
    public void BeginGame()
    {
        _instructionsUI.SetActive(false);
        GameManager.Instance.IsPaused = false;
    }
    public void EndGame(bool win)
    {
        _endGameUI.ShowEndGame(win);
        StartCoroutine(EndGameWait());
    }
    IEnumerator EndGameWait()
    {
        yield return new WaitForSeconds(2);
        GameManager.Instance.IsPaused = true;
    }
}
