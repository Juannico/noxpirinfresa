using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public int amountOfEnemiesToKill = 3;
    [SerializeField] private CollectableData _collectableData;
    [SerializeField] private int _maxCollectableAmount = 9;
    private static GameManager _instance;
    public static GameManager Instance => _instance;

    public delegate void BoolChanged(bool newBoolValue);
    public BoolChanged OnPauseChange;
    private bool _isPaused;
    public bool IsPaused
    {
        get => _isPaused;
        set
        {
            _isPaused = value;
            Time.timeScale = _isPaused ? 0 : 1;
            OnPauseChange.Invoke(_isPaused);
        }
    }
    private int _enemiesKilled;
    public int EnemiesKilled
    {
        get => _enemiesKilled;
        set
        {
            _enemiesKilled = value;
            if (_enemiesKilled >= amountOfEnemiesToKill)
                StartCoroutine(HoldBeforeEndScreen());
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }
        _instance = this;
        DontDestroyOnLoad(this);
        _collectableData.OnAmountChanged += CheckCollectableAmount;

    }
    private void Start()
    {
        IsPaused = true;
    }
    private void CheckCollectableAmount(int collectableAmount)
    {
        if (collectableAmount > 0 || _collectableData.amountCollected < 9)
            return;
        UiManager.Instance.EndGame(false);
    }
    public void RestartGame()
    {
        Destroy(UiManager.Instance.gameObject);
        _enemiesKilled = 0;
        _collectableData.RestartData();
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
    IEnumerator HoldBeforeEndScreen()
    {
        yield return new WaitForSeconds(1);
        UiManager.Instance.EndGame(true);
    }
}
