using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] private LayerData _layerData;
    [SerializeField] private CollectableData _recollectableData;
    [SerializeField] private Projectile _projectile;
    [SerializeField] private Animator _cameraAnimator;
    [SerializeField] private Transform _barrel;
    [SerializeField] private float _maxAngle = 180;
    [SerializeField] private float offsetProjectil = 1.25f;
    [Header("Fx")]
    [SerializeField] private ParticleSystem _shootEffect;
    [SerializeField] private AudioSource _shootSoundEffect;
    private bool _towerActivated;
    private Vector2 _direction;
    private bool _loadingAttack;
    private Vector2 _maxDirection;
    private SpriteRenderer _barrelSpriteRenderer;
    private float _angle;
    private void Awake()
    {
        _angle = _maxAngle * 0.5f;
        _maxDirection = MathUtilities.RotateVector(Vector2.up, _angle);
        _barrelSpriteRenderer = _barrel.GetComponent<SpriteRenderer>();
    }
    private void Update()
    {
        if (!_loadingAttack)
            return;
        Aim();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!LayerUtilities.IsSameLayer(_layerData.playerLayer, other.gameObject.layer))
            return;
        UiManager.Instance.SetEnterTowwerButtonState(true);
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (!LayerUtilities.IsSameLayer(_layerData.playerLayer, other.gameObject.layer))
            return;
        UiManager.Instance.SetEnterTowwerButtonState(false);
    }
    private void OnDestroy()
    {
        InputManager.Instance.shoot.action.started -= LoadShootCallBackContext;
        InputManager.Instance.shoot.action.canceled -= ShootCallBackContext;
    }
    private void Aim()
    {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(_barrel.position);
        _direction = InputManager.Instance.pointPosition.action.ReadValue<Vector2>() - screenPosition;
        bool leftSide = _direction.x > 0;
        _barrelSpriteRenderer.flipY = leftSide;
        LimitRotation(leftSide);
        _projectile.transform.position = _barrel.position + (Vector3)_direction.normalized * offsetProjectil;
        _projectile.transform.up = _direction;
        _barrel.right = -_direction;
    }
    private void LimitRotation(bool leftSide)
    {
        if (Vector2.Angle(Vector2.up, _direction) <= _angle)
            return;
        _direction = _maxDirection;
        if (leftSide)
            _direction.x *= -1;
    }
    private void LoadShootCallBackContext(UnityEngine.InputSystem.InputAction.CallbackContext ctx)
    {
        if (_projectile._spriteRenderer.enabled || _loadingAttack)
            return;
        if (_recollectableData.Amount == 0)
        {
            Debug.Log("Not enougth ammo");
            return;
        }
        _loadingAttack = true;
        _projectile.Prepare();
    }
    private void ShootCallBackContext(UnityEngine.InputSystem.InputAction.CallbackContext ctx)
    {
        if (!_loadingAttack)
            return;
        _projectile.Throw(_direction.normalized, () => _recollectableData.Amount--, _projectile.transform.position);
        _loadingAttack = false;
        _shootEffect.Play();
        _shootEffect.transform.position = _projectile.transform.position;
        _shootSoundEffect?.Play();
    }
    public void SwithchTowerState()
    {
        _towerActivated = !_towerActivated;
        if (_towerActivated)
        {
            InputManager.Instance.shoot.action.started += LoadShootCallBackContext;
            InputManager.Instance.shoot.action.canceled += ShootCallBackContext;
        }
        else
        {
            InputManager.Instance.shoot.action.started -= LoadShootCallBackContext;
            InputManager.Instance.shoot.action.canceled -= ShootCallBackContext;
            if (_loadingAttack)
            {
                _projectile.StopTrow();
                _loadingAttack = false;
            }
        }
        _cameraAnimator.SetBool("Tower", _towerActivated);
        InputManager.Instance.SetActionState(InputManager.Instance.move, !_towerActivated);
    }
}
