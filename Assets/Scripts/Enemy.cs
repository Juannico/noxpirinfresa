using UnityEngine;
using UnityEngine.Rendering.Universal;

public class Enemy : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [Header("Fx")]
    [SerializeField] private ParticleSystem _diedEffect;
    [SerializeField] private AudioSource _diedSoundEffect;

    public void Kill()
    {
        _spriteRenderer.enabled = false;
        _diedEffect?.Play();
        _diedSoundEffect?.Play();
        gameObject.GetComponent<Collider2D>().enabled = false;
        gameObject.GetComponent<ShadowCaster2D>().enabled = false;
        GameManager.Instance.EnemiesKilled++;
    }
}
