using UnityEngine;

public class Collectable : MonoBehaviour
{
    [SerializeField] private LayerData _layerData;
    [SerializeField] private CollectableData _recollectableData;
    [SerializeField] private SpriteRenderer _spriteRender;
    [SerializeField] private float _respawnTime;
    [SerializeField] private float _maxRespawnsAmount = 3;
    [Header("Fx")]
    [SerializeField] private ParticleSystem _collectEffect;
    private AudioSource collectSound;
    private bool _isCollected;
    private int _respawnTimes;
    private void Awake()
    {
        collectSound = GetComponent<AudioSource>();
        _spriteRender.sprite = _recollectableData.sprite;
        Restart();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_isCollected)
            return;
        if (!LayerUtilities.IsSameLayer(_layerData.playerLayer, other.gameObject.layer))
            return;
        if (!_recollectableData.CanCollect())
            return;
        _isCollected = true;
        _recollectableData.Amount++;
        _spriteRender.enabled = false;
        _collectEffect?.Play();
        collectSound.Play();
        _respawnTimes++;
        if (_respawnTimes < _maxRespawnsAmount)
            CoroutineHandler.ExecuteActionAfter(Restart, _respawnTime, this);
    }
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (_spriteRender == null || _recollectableData == null)
            return;
        _spriteRender.sprite = _recollectableData.sprite;
    }
#endif
    private void Restart()
    {
        _isCollected = false;
        _spriteRender.enabled = true;
    }
}
