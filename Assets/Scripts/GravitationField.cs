using UnityEngine;

public class GravitationField : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed;
    [SerializeField] private float _projectileMaxAngleRotation = 15;
    private Projectile _currentProjectile;
    private bool _projectileEntered;
    private Quaternion _targetRotation;
    private void Awake()
    {
        InputManager.Instance.shoot.action.performed += ExitProyectileCallBackContext;
    }
    private void Update()
    {
        transform.Rotate(Vector3.forward * _rotationSpeed * Time.deltaTime);
        if (!_projectileEntered)
            return;
        if (_currentProjectile.transform.localRotation != _targetRotation)
            _currentProjectile.transform.localRotation = Quaternion.RotateTowards(_currentProjectile.transform.localRotation, _targetRotation, _projectileMaxAngleRotation);
    }
    private void OnDestroy()
    {
        InputManager.Instance.shoot.action.performed -= ExitProyectileCallBackContext;
    }
    public void ProjectileEnter(Projectile projectile)
    {
        if (_projectileEntered)
            return;
        _projectileEntered = true;
        projectile.PauseTrow();
        _currentProjectile = projectile;
        projectile.transform.parent = transform;
        Vector2 localPosition = projectile.transform.localPosition;
        float angle = Mathf.Atan2(localPosition.y, localPosition.x) * Mathf.Rad2Deg;
        if (_rotationSpeed < 0)
            angle += 180;
        _targetRotation = Quaternion.Euler(0, 0, angle);
    }
    private void ExitProyectileCallBackContext(UnityEngine.InputSystem.InputAction.CallbackContext ctx) => ExitProyectile();
    private void ExitProyectile()
    {
        if (!_projectileEntered)
            return;
        _projectileEntered = false;
        _currentProjectile.transform.parent = null;
        _currentProjectile.Throw(_currentProjectile.transform.up);
    }
}
