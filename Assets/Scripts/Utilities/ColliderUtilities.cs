using UnityEngine;

public static class ColliderUtilities
{
    /// <summary>
    /// Calculate the normal vector of object collider2D.
    /// </summary>
    /// <param name="other">The Collider2D for which to calculate the normal.</param>
    /// <param name="objectPosition">The position of the object Collider2D.</param>
    /// <returns>The normal of the collider contact.</returns>
    public static Vector2 Collider2DNormal(Collider2D other, Vector2 objecPosition)
    {
        Vector2 closestPoint = other.ClosestPoint(objecPosition);
        return (objecPosition - closestPoint).normalized;
    }

}
