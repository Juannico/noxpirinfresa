using UnityEngine;

public static class MathUtilities 
{
    /// <summary>
    /// Rotate vector with given angle.
    /// </summary>
    /// <param name="originalVector">The vector to be rotated.</param>
    /// <param name="angle">The angle of rotation.</param>
    /// <returns>The rotated vector.</returns>
    public static Vector2 RotateVector(Vector2 originalVector,float angle)
    {
        Quaternion rotation = Quaternion.Euler(0f, 0f, angle);
        return rotation * originalVector;
    }
    /// <summary>
    /// Calculate the cross product of two 2D vectors.
    /// </summary>
    /// <param name="fromVector">The first vector.</param>
    /// <param name="toVector">The second vector.</param>
    /// <returns>The cross product result (scalar).</returns>
    public static float Vector2CrossProtuct(Vector2 fromVector, Vector2 toVector) => fromVector.x * toVector.y - fromVector.y * toVector.x;
}
