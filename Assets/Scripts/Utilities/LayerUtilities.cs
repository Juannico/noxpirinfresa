using UnityEngine;

public static class LayerUtilities 
{
    /// <summary>
    /// Check if a specific layer is present in a LayerMask.
    /// </summary>
    /// <param name="layerMask">The LayerMask to check.</param>
    /// <param name="intLayer">The layer to check for.</param>
    /// <returns>True if the layer is present in the LayerMask, false otherwise.</returns>
    public static bool IsSameLayer(LayerMask layerMask, int intLayer) => ContainsLayer(layerMask, intLayer);
    private static bool ContainsLayer(LayerMask layerMask, int layer) => (1 << layer & layerMask) != 0;
   
}
