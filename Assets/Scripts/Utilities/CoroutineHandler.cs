using System;
using System.Collections;
using UnityEngine;

public static class CoroutineHandler
{
    /// <summary>
    /// Execute action after a delay time with Unity TimeScale.
    /// </summary>
    /// <param name="action">Method to execute after the specified time.</param>
    /// <param name="duration">Delay time before executing the method.</param>
    /// <param name="objectCalling">MonoBehaviour that calls the coroutine.</param>
    public static void ExecuteActionAfter(Action action, float duration, MonoBehaviour objectCalling)
    {
        objectCalling.StartCoroutine(ExecuteActionAfterEnumerator(action, duration));
    }
    /// <summary>
    /// Execute action after a delay time, ignoring Unity TimeScale.
    /// </summary>
    /// <param name="action">Method to execute after the specified time.</param>
    /// <param name="duration">Delay time before executing the method.</param>
    /// <param name="objectCalling">MonoBehaviour that calls the coroutine.</param>
    public static void ExcuteActionAfterUnescaledTime(Action action, float duration, MonoBehaviour objectCalling)
    {
        objectCalling.StartCoroutine(ExecuteActionAfterEnumerator(action, duration, true));
    }

    /// <summary>
    /// This method will execute a coroutine passed by parameter after the duration passed by parameter.
    /// </summary>
    /// <param name="action">Method to execute after the specified time.</param>
    /// <param name="duration">Delay time before executing the method.</param>
    /// <param name="useUnescaledTime">Flag indicating whether to ignore Unity TimeScale.</param>
    /// <returns></returns>
    public static IEnumerator ExecuteActionAfterEnumerator(Action action, float duration, bool useUnescaledTime = false)
    {
        if (!useUnescaledTime)
        {
            yield return new WaitForSeconds(duration);
            action?.Invoke();
            yield break;
        }
        float startTime = Time.realtimeSinceStartup;
        float elapsedTime = 0f;

        while (elapsedTime < duration)
        {
            elapsedTime = Time.realtimeSinceStartup - startTime;
            yield return null;
        }
        action?.Invoke();
    }
}
