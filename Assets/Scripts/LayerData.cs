using UnityEngine;

[CreateAssetMenu(fileName = "SO_LayerData", menuName = "ScriptableObjects/LayerData", order = 1)]
public class LayerData : ScriptableObject
{
    public LayerMask playerLayer;
    public LayerMask enviromentLayer;
    public LayerMask obstacleLayer;
}
