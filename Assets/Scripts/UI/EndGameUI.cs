using UnityEngine;
using UnityEngine.UI;

public class EndGameUI : MonoBehaviour
{
    [SerializeField] private Text _endText;
    [SerializeField] private GameObject[] _stars;
    [SerializeField] private Button _restartButton;
    private bool _endGame;
    private Animator _animator;
    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _restartButton.onClick.AddListener(GameManager.Instance.RestartGame);
        if (_endGame)
            return;
        gameObject.SetActive(false);
        for (int i = 0; i < _stars.Length; i++)
            _stars[i].SetActive(false);
    }
    public void ShowEndGame(bool win)
    {
        _endGame = true;
        gameObject.SetActive(true);
        _animator.SetTrigger("endgame");
        _endText.text = win ? "WIN" : "LOSE";
        for (int i = 0; i < _stars.Length; i++)
            _stars[i].SetActive(i < GameManager.Instance.EnemiesKilled);
    }
}
