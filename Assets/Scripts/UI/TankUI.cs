using UnityEngine;
using UnityEngine.UI;

public class TankUI : MonoBehaviour
{
    [SerializeField] private CollectableData _collectableData;
    [SerializeField] private Image _tankFill;
    [SerializeField] private float _fillSpeed = 0.25f;

    private float _refVelocity;
    private float _targetFillAmount;

    private void Awake()
    {
        _targetFillAmount = _collectableData.Amount;
        _tankFill.fillAmount = _collectableData.Amount;
        _collectableData.OnAmountChanged += UpdateTank;
    }
    private void Update()
    {
        if (_tankFill.fillAmount == _targetFillAmount)
            return;
        _tankFill.fillAmount = Mathf.SmoothDamp(_tankFill.fillAmount, _targetFillAmount, ref _refVelocity, _fillSpeed);
    }
    private void OnDestroy()
    {
        _collectableData.OnAmountChanged -= UpdateTank;
    }
    private void UpdateTank(int collectableAmount)
    {
        _targetFillAmount = (float)collectableAmount / _collectableData.maxAmountInPossesion;
    }
}
